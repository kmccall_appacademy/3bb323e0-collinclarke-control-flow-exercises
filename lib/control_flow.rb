# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
characters = str.chars
characters.each do |ch|
  if ch == ch.downcase
    str.delete!(ch)
  end
end
str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
 middle = str.length / 2
  if str.length.odd?
    return str[middle]
  else
    str[(middle - 1)..middle]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
 str.chars.each do |ch|
   if VOWELS.include?(ch)
     count += 1
   end
 end
 count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
 (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  if arr.length == 0
    return ""
  end
    arr.reduce do |acc, el|
      acc += separator
      acc += el
    end
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)

 chars = str.chars
 count = 1
 chars.map do |ch|
   if count.odd?
     ch.downcase!
   else
     ch.upcase!
   end
   count += 1
 end
 chars.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
 words = str.split(' ')
 words.map do |word|
   if word.length >= 5
   word.reverse!
   end
 end
 words.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
 arr = (1..n).to_a
 arr.each_with_index do |int, idx|
   if int % 3 == 0 && int % 5 == 0
     arr[idx] = 'fizzbuzz'
   elsif int % 3 == 0
     arr[idx] = 'fizz'
   elsif int % 5 == 0
     arr[idx] = 'buzz'
   end
 end
 arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reverse = []
 arr.each do |el|
   reverse.unshift(el)
 end
 reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  end
  (2..(num/2)).each do |int|
    if num % int == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
 (1..(num/2)).each do |int|
   if num % int == 0
     factors << int
   end
 end
  factors << num
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
prm_fct = []
 factors(num).each do |num|
   if prime?(num)
     prm_fct << num
   end
 end
 prm_fct
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
 prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
 odd = []
 even = []
 arr.each do |int|
   if int.odd?
     odd << int
   else
     even << int
   end
 end
 if odd.length > even.length
   return even[0]
 else
   return odd[0]
 end
end
